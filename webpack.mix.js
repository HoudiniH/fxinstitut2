const mix = require('laravel-mix');
const webpack = require('webpack');

mix.webpackConfig({
    plugins: [
        // Fix for moment.js failed to find local error
        new webpack.ContextReplacementPlugin(/\.\/locale$/, 'empty-module', false, /js$/)
    ]
});
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public');

// mix.copy('resources/sass/plugins/fonts', 'public/css/fonts')
//     .sass('resources/sass/style.scss', 'public/css')
//     .js('resources/js/app.js', 'public/js/app.js').vue()
//     .copy('resources/js/def/main.js', 'public/js/main.js')
// mix.autoload({
//     jquery: ['$', 'jQuery', 'window.jQuery', 'window.$', 'jquery']
// });

mix.js('resources/js/frontend/app_ecommerce.js', 'assets/js').vue()
    .sass('resources/scss/frontend/app.scss', 'assets/css').version()
    // .sourceMaps()
    .sass('resources/scss/backend/app.scss', 'assets/css/backend.css')
    .copy('node_modules/font-awesome/fonts', 'public/assets/fonts')
    .copy('node_modules/font-awesome/css/font-awesome.css', 'public/assets/css/font-awesome.css')
    .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap', 'public/assets/fonts/bootstrap')
    .js('resources/js/frontend/app.js', 'assets/js')
    .js('resources/js/frontend/script.js', 'assets/js')
    .js([
        'resources/js/backend/before.js',
        'resources/js/backend/app.js',
        'resources/js/backend/after.js'
    ], 'assets/js/backend.js')
// .extract([
//     'jquery',
//     'jquery-ui-dist/jquery-ui',
//     'bootstrap',
//     'popper.js/dist/umd/popper',
//     'axios',
//     'sweetalert2',
// ]);
// .autoload({
//     jquery: ['$', 'jQuery', 'window.jQuery', 'window.$', 'jquery']
// })
// .extract([
//     'jquery',
//     'popper.js',
//     'bootstrap',
//     'owl.carousel',
//     'jarallax',
//     'magnific-popup',
//     'lightbox2',
//     'scrollreveal',
//     'counterup',
//     'waypoints',
//     'jquery-ui-dist',
//     'axios',
//     'sweetalert2',
// ]);
// .browserSync('http://fxinstitut.test')
//************** SCRIPTS ******************
// general scripts
// mix.copy('resources/scss/frontend/plugins/fonts', 'public/assets/css/fonts')
// mix.copy('node_modules/@coreui/coreui/dist/js/coreui-utilities.js', 'public/assets/js/coreui');
// mix.copy('node_modules/axios/dist/axios.min.js', 'public/assets/js/axios');
// mix.copy('node_modules/@coreui/coreui/dist/js/coreui.min.js', 'public/assets/js/coreui');
// views scripts
// mix.copy('node_modules/chart.js/dist/Chart.min.js', 'public/js');
// mix.copy('node_modules/@coreui/chartjs/dist/js/coreui-chartjs.bundle.js', 'public/js');

// mix.copy('node_modules/cropperjs/dist/cropper.js', 'public/js');
// details scripts
// mix.copy('resources/js/coreui/main.js', 'public/assets/js/coreui');
// mix.copy('resources/js/coreui/colors.js', 'public/assets/js/coreui');
// mix.copy('resources/js/coreui/charts.js', 'public/assets/js/coreui');
// mix.copy('resources/js/coreui/widgets.js', 'public/assets/js/coreui');
// mix.copy('resources/js/coreui/popovers.js', 'public/assets/js/coreui');
// mix.copy('resources/js/coreui/tooltips.js', 'public/assets/js/coreui');
// details scripts admin-panel
// mix.js('resources/js/coreui/menu-create.js', 'assets/js/coreui');
// mix.js('resources/js/coreui/menu-edit.js', 'assets/js/coreui');
// mix.js('resources/js/coreui/media.js', 'assets/js/coreui');
// mix.js('resources/js/coreui/media-cropp.js', 'assets/js/coreui');

//images
// mix.copy('resources/assets', 'public/assets');

if (mix.inProduction() || process.env.npm_lifecycle_event !== 'hot') {
    mix.version();
}
