<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	use HasFactory;
    protected $table = "images";
    protected $guarded = [];

    public function model()
    {
        return $this->morphTo();
    }


}
// $year = date("Y");
// $month = date("m");
// $filename = "uploads/".$year;
// $filename2 = "uploads/".$year."/".$month;

// if(file_exists($filename)){
//     if(file_exists($filename2)==false){
//         mkdir($filename2,777, true);
//         }
// }else{
//     mkdir($filename, 777,true);
//         mkdir($filename2,777, true);
// }
