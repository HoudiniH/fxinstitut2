<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;


    public function getLogoAttribute()
    {
        return $this->image;
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

}
