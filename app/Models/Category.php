<?php

namespace App\Models;

use App\Models\Formation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
	use HasFactory, Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'slug', 'icon', 'status'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [];

    public function formations(){
        return $this->hasMany(Formation::class);
    }

    public function tutorials(){
        return $this->hasMany(Tutorial::class);
    }

    public function bundles(){
        return $this->hasMany(Bundle::class);
    }

    public function blogs(){
        return $this->hasMany(Blog::class);
    }

    public function faqs(){
        return $this->hasMany(Faq::class);
    }

    public function portfolios(){
        return $this->hasMany(Portfolio::class);
    }

    public function tipstricks(){
        return $this->hasMany(Tipstrick::class);
    }

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }


    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }


}
