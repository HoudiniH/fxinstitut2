<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ShopController extends Controller
{
        /**
     * @var Product
     */
    protected $products;

    /**
     * HomeController constructor.
     * @param Product $products
     */
    function __construct(Product $products)
    {
        $this->products = $products;
    }

    /**
     * Displays a list of products to the user
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $products = $this->products->with(['sales' => function($query){
                return $query->current();
            }])->paginate(9);
        } catch (\Exception $exception) {
            $exception->getMessage();
        }

        return view('frontend.ecommerce.shop', [
            'products' => $products
        ]);
    }
}
