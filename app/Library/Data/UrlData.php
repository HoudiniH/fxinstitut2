<?php
/*
|--------------------------------------------------------------------------
| UrlData.php
|--------------------------------------------------------------------------
| Created by Shawn Legge
| This class is responsible for returning a list of common urls that will be
| used by vue
*/

namespace App\Library\Data;


use App\Http\Controllers\Auth\RegisterValidationController;



class UrlData
{
    public static function get()
    {
        return [
            'state_url' => route('shopping.user.states'),
            'address_url' => route('address.index'),
            'order_url' => route('shopping.order.add.api'),
            'billing_url' => route('shopping.order.billing.post'),
            'users_url' => route('shopping.users.details'),
            'user_order_url' => str_replace('/:id', '', route('shopping.user.account.order', [':id'])),
            'search_url' => str_replace('/:search', '', route('shopping.search.products.search', [':search'])),
            'search_url_api' => route('shopping.search.products.api'),

            'show_product_url' => str_replace('/:product', '', route('shopping.product.show', [':product'])),

            'shopping_cart' => route('shopping.cart'),
            'shopping_cart_add' => route('shopping.cart.add'),
            'shopping_cart_delete' => route('shopping.cart.destroy'),
            'shopping_cart_update' => route('shopping.cart.update'),
            'category_url' => str_replace('/:category', '', route('shopping.search.product.category', [':category'])),
            // 'user_email_url' => action([RegisterValidationController::class, 'email']),

            // 'user_username_url' => action([RegisterValidationController::class, 'username'])
        ];
    }
}

