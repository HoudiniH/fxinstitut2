<?php
/*
|--------------------------------------------------------------------------
| AdminUrlData.php
|--------------------------------------------------------------------------
| Created by Shawn Legge
| This class is responsible for returning a list of common urls that will be
| used by vue in the admin site
*/

namespace App\Library\Data;

use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\API\StatesAPIController;

use App\Http\Controllers\Admin\API\CategoriesAPIController;

use App\Http\Controllers\Admin\CategoriesController;

use App\Http\Controllers\Admin\API\OrdersAPIController;

use App\Http\Controllers\Admin\SalesController;

use App\Http\Controllers\Admin\API\TaxesAPIController;

use App\Http\Controllers\Admin\TaxesController;

use App\Http\Controllers\Admin\API\ProductsAPIController;



class AdminUrlData
{
    public static function get()
    {
        return [
            'product_url' => route('admin.products.index'),
            'product_api_url' => route('admin.products.api'),
            'tax_url' => route('admin.taxes'),
            'tax_api_url' => route('admin.taxes.index'),
            'sale_url' => route('admin.sales.index'),
            'order_url' => str_replace('/:order', '', route('admin.order.show.api', [':order'])),
            'category_url' => route('admin.categories'),
            'category_api_url' => route('admin.categories.index'),
            'state_api_url' => route('admin.states.index')
        ];
    }
}
