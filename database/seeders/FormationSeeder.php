<?php

namespace Database\Seeders;

use App\Models\Test;
use App\Models\Formation;
use App\Models\Lesson;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Models\Image;

class FormationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {

        //Adding Categories
        \App\Models\Category::factory(10)->create()->each(function ($cat) {
            $cat->blogs()->saveMany(\App\Models\Blog::factory(4)->create());

        });

        //Creating Formation
        Formation::factory(50)->create()->each(function ($formation) {

            $formation->teachers()->sync([2]);
            // dd($formation->id);
            $formation->lessons()->saveMany(Lesson::factory(10)->create());

            $placeholder = ['placeholder-1.jpg', 'placeholder-2.jpg', 'placeholder-3.jpg' ];

            $name = $placeholder[rand(0, 2)];

            $image = Image::create([
                'model_type' => 'App\Models\Formation',
                'model_id' => $formation->id,
                'name' => $name,
                'file_name' => $name,
                'url' => asset('storage/uploads/fmts/' . $name)
            ]);

            $formation->image_id = $image->id;
            $formation->save();

            foreach ($formation->lessons()->where('published', '=', 1)->get() as $key => $lesson) {
                $key++;
                $timeline = new \App\Models\FormationTimeline();
                $timeline->formation_id = $formation->id;
                $timeline->model_id = $lesson->id;
                $timeline->model_type = Lesson::class;
                $timeline->sequence = $key;
                $timeline->save();
            };

            $formation->tests()->saveMany(Test::factory(2)->create());
            foreach ($formation->tests as $key => $test) {
                $key += 11;
                $timeline = new \App\Models\FormationTimeline();
                $timeline->formation_id = $formation->id;
                $timeline->model_id = $test->id;
                $timeline->model_type = Test::class;
                $timeline->sequence = $key;
                $timeline->save();
            };
        });

        $formations = Formation::get()->take(3);

        foreach ($formations as $formation) {
            $lesson_id = $formation->formationTimeline->where('sequence', '=', 1)->first()->model_id;
            $lesson = Lesson::find($lesson_id);
            $media = \App\Models\Media::where('type', '=', 'upload')
            ->where('model_type', '=', 'App\Models\Lesson')
            ->where('model_id', '=', $lesson->id)
            ->first();
            $filename = 'placeholder-video.mp4';
            $url = asset('storage/uploads/fmts/' . $filename);

            if ($media == null) {
                $media = new \App\Models\Media();
                $media->model_type = Lesson::class;
                $media->model_id = $lesson->id;
                $media->name = $filename;
                $media->url = $url;
                $media->type = 'upload';
                $media->file_name = $filename;
                $media->size = 0;
                $media->save();
            }

            $order = new \App\Models\Order();
            $order->user_id = 3;
            $order->reference_no = Str::random(8);
            $order->amount = $formation->price;
            $order->status = 1;
            $order->save();

            $order->items()->create([
                'item_id' => $formation->id,
                'item_type' => get_class($formation),
                'price' => $formation->price
            ]);
            generateInvoice($order);

            foreach ($order->items as $orderItem) {
                $orderItem->item->students()->attach(3);
            }
        }


        //Creating Bundles
        \App\Models\Bundle::factory(10)->create()->each(function ($bundle) {
            $bundle->user_id = 2;
            $bundle->save();
            $bundle->formations()->sync([ rand(1,50) , rand(1,50) , rand(1,50)  ]);
        });


        $bundles = \App\Models\Bundle::get()->take(2);

        foreach ($bundles as $bundle){
            $order = new \App\Models\Order();
            $order->user_id = 3;
            $order->reference_no = Str::random(8);
            $order->amount = $bundle->price;
            $order->status = 1;
            $order->save();

            $order->items()->create([
                'item_id' => $bundle->id,
                'item_type' => get_class($bundle),
                'price' => $bundle->price
            ]);
            generateInvoice($order);

            foreach ($order->items as $orderItem) {
                //Bundle Entries
                if($orderItem->item_type == \App\Models\Bundle::class){
                    foreach ($orderItem->item->formations as $formation){
                        $formation->students()->attach($order->user_id);
                    }
                }
                $orderItem->item->students()->attach($order->user_id);
            }
        }
    }
}
