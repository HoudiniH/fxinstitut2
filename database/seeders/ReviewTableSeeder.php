<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory('App\Models\Review', 150)->create();
        $reviews = \App\Library\Data\FetchJsonFile::open('reviews.json');

        foreach ($reviews as $review)
        {
            $product = \App\Models\Product::inRandomOrder()->first();
            $formation = \App\Models\Formation::inRandomOrder()->first();

            $model = array( $product, $formation);
            $key = array_rand( $model);
            $model = $model[$key];




            $user = \App\Models\Auth\User::inRandomOrder()->first();
            // dd($model);
            \App\Models\Review::create([
               'reviewable_id' => $model->id,
               'reviewable_type' => '\App\Models\\' . get_class($model),
               'user_id' => $user->id,
               'stars' => $review['stars'],
               'content' => $review['review']
            ]);
        }
    }
}
