<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use  Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class V2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'bundle_access',
            'bundle_create',
            'bundle_edit',
            'bundle_view',
            'bundle_delete'
        ];
        $permission_ids = [];


        foreach ($permissions as $item) {
         Permission::findOrCreate($item);
     }
     Artisan::call('cache:clear');

     $admin = Role::findByName('administrator');
     $admin->givePermissionTo($permissions);
     $teacher =Role::findByName('teacher');
     $teacher->givePermissionTo($permissions);

     $student =Role::findByName('student');
     $student->givePermissionTo(['bundle_view','bundle_access']);


     $menus = [
        [
            'url' => 'bundles',
            'name' => 'Bundles'
        ],
    ];



        //=========Order fix ===================//

    \App\Models\OrderItem::where('item_type','=',NULL)->update(['item_type'=>"\App\Models\Formation"]);

}
}
