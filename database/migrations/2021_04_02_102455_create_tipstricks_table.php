<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipstricksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipstricks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('snipet');
            $table->text('content');
            $table->integer('media');
            $table->foreignId('category_id')->constrained('categories')->nullable()->onDelete('cascade');
            $table->timestamps();

            // $table->foreign('category_id')
            //     ->references('id')
            //     ->on('categories')
            //     ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipstricks');
    }
}
