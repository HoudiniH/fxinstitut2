<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignId('image_id')->nullable()->constrained('images')->onDelete('cascade');
            $table->string('slug');
            $table->string('media')->nullable();
            $table->string('url')->nullable();
            $table->text('description')->nullable();
            $table->integer('category_id')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->date('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
