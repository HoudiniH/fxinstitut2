<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffings', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('destination');
            $table->string('role');
            $table->string('periode');
            $table->text('content');
            $table->string('location')->nullable();
            $table->tinyInteger('published')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffings');
    }
}
