<?php

namespace Database\Factories;

use App\Models\Image;
use App\Models\Formation;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class FormationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Formation::class;

    protected $images = array('placeholder-1.jpg', 'placeholder-2.jpg', 'placeholder-3.jpg');
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        // $products = \App\Library\Data\FetchJsonFile::open('products.json');
        // $paths = $this->images;

        // $placeholders = ['placeholder-1.jpg','placeholder-2.jpg','placeholder-3.jpg'];

        // $images = [];

        // foreach ($paths as $path)
        // {
        //     // if(count($paths) > 0) {
        //     //     $path = array_pop($paths);
        //     // } else {
        //     //     $paths = $this->images;
        //     //     $path = array_pop($paths);
        //     // }

        //     // $image = \App\Models\Image::create([
        //     //     'model_type' => get_class(new Formation()),
        //     //     'model_id' => $product['id'],
        //     //     'name' => $path,
        //     //     'file_name' => $path,
        //     //     'url' => asset('storage/uploads/fmts/'.$path),
        //     //     'size' => 0,
        //     // ]);
        // }


        $name = $this->faker->sentence(5);
        $placeholder = ['placeholder-1.jpg','placeholder-2.jpg','placeholder-3.jpg'];
        return [
            'title' => $name,
            'category_id' => rand(1,10),
            'slug' => Str::slug($name),
            // 'image_id' => $placeholder[rand(0, 2)],
            'description' => $this->faker->text(),
            'price' => $this->faker->randomFloat(2, 0, 199),
            'featured' => Arr::random([0,1]),
            'trending' => Arr::random([0,1]),
            'popular' => Arr::random([0,1]),
            'published' => 1,
        ];
    }
}
