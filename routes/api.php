<?php

namespace Routes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\v1\ApiController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::prefix('v1')->group( function (){


    Route::prefix('auth')
        ->namespace('v1')
        ->group(function () {

    //      Route::post('login', [ApiController::class, 'login']);
            Route::post('signup-form', [ApiController::class, 'signupForm']);
            Route::post('signup-save', [ApiController::class, 'signup']);

            Route::middleware(['auth:api'])->group( function() {

                Route::post('logout', [ApiController::class, 'logout']);

        });
    });

    Route::middleware(['auth:api'])
        ->namespace('v1')
        ->group( function (){

        Route::post('formations', [ApiController::class, 'getFormations']);
        Route::post('bundles', [ApiController::class, 'getBundles']);
        Route::post('search', [ApiController::class, 'search']);
        Route::post('latest-news', [ApiController::class, 'getLatestNews']);
        Route::post('teachers', [ApiController::class, 'getTeachers']);
        Route::post('single-teacher', [ApiController::class, 'getSingleTeacher']);
        Route::post('teacher-formations', [ApiController::class, 'getTeacherFormations']);
        Route::post('teacher-bundles', [ApiController::class, 'getTeacherBundles']);
        Route::post('get-faqs', [ApiController::class, 'getFaqs']);
        Route::post('sponsors', [ApiController::class, 'getSponsors']);
        Route::post('contact-us', [ApiController::class, 'saveContactUs']);
        Route::post('single-formation', [ApiController::class, 'getSingleFormation']);
        Route::post('submit-review', [ApiController::class, 'submitReview']);
        Route::post('update-review', [ApiController::class, 'updateReview']);
        Route::post('single-lesson', [ApiController::class, 'getLesson']);
        Route::post('video-progress', [ApiController::class, 'videoProgress']);
        Route::post('formation-progress', [ApiController::class, 'formationProgress']);
        Route::post('generate-certificate', [ApiController::class, 'generateCertificate']);
        Route::post('single-bundle', [ApiController::class, 'getSingleBundle']);
        Route::post('add-to-cart', [ApiController::class, 'addToCart']);
        Route::post('getnow', [ApiController::class, 'getNow']);
        Route::post('remove-from-cart', [ApiController::class, 'removeFromCart']);
        Route::post('get-cart-data', [ApiController::class, 'getCartData']);
        Route::post('clear-cart', [ApiController::class, 'clearCart']);
        Route::post('payment-status', [ApiController::class, 'paymentStatus']);
        Route::post('get-blog', [ApiController::class, 'getBlog']);
        Route::post('blog-by-category', [ApiController::class, 'getBlogByCategory']);
        Route::post('blog-by-tag', [ApiController::class, 'getBlogByTag']);
        Route::post('add-blog-comment', [ApiController::class, 'addBlogComment']);
        Route::post('delete-blog-comment', [ApiController::class, 'deleteBlogComment']);
        Route::post('forum', [ApiController::class, 'getForum']);
        Route::post('create-discussion', [ApiController::class, 'createDiscussion']);
        Route::post('store-response', [ApiController::class, 'storeResponse']);
        Route::post('update-response', [ApiController::class, 'updateResponse']);
        Route::post('delete-response', [ApiController::class, 'deleteResponse']);
        Route::post('messages', [ApiController::class, 'getMessages']);
        Route::post('compose-message', [ApiController::class, 'composeMessage']);
        Route::post('reply-message', [ApiController::class, 'replyMessage']);
        Route::post('unread-messages', [ApiController::class, 'getUnreadMessages']);
        Route::post('search-messages', [ApiController::class, 'searchMessages']);
        Route::post('my-certificates', [ApiController::class, 'getMyCertificates']);
        Route::post('my-purchases', [ApiController::class, 'getMyPurchases']);
        Route::post('my-account', [ApiController::class, 'getMyAccount']);
        Route::post('update-account', [ApiController::class, 'updateMyAccount']);
        Route::post('update-password', [ApiController::class, 'updatePassword']);
        Route::post('get-page', [ApiController::class, 'getPage']);
        Route::post('subscribe-newsletter', [ApiController::class, 'subscribeNewsletter']);
        Route::post('offers', [ApiController::class, 'getOffers']);
        Route::post('apply-coupon', [ApiController::class, 'applyCoupon']);
        Route::post('remove-coupon', [ApiController::class, 'removeCoupon']);
        Route::post('order-confirmation', [ApiController::class, 'orderConfirmation']);
    });
    Route::middleware(['auth:api'])
        ->namespace('v1')
        ->group( function (){

        Route::post('send-reset-link', [ApiController::class]);

    });
    Route::middleware(['auth:api'])
        ->namespace('v1')
        ->group( function (){

        Route::post('configs', [ApiController::class, 'getConfigs']);

    });
});

