<footer class="app-footer">
        <div class="d-block w-100 text-center"><strong>@lang('labels.general.copyright') &copy; {{ date('Y') }} <a target="_blank" href="{{ route('frontend.index')}}">FXinstitut</a></strong> @lang('strings.backend.general.all_rights_reserved') <p class="float-right mb-0">Powered By <a href="{{ route('frontend.index')}}" target="_blank" class="mr-4"> FXinstitut</a></p></div>

</footer>
