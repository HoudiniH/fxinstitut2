@if(is_array($item) )
    <li>
        <a class="" id="menu-{{$item}}" href="{{$item->link}}">{{trans('custom-menu.'.$menu_name.'.'.Str::slug($item->label))}}</a>
        <ul class="depth-1">
            @foreach($item->subs as $item)
                @include('frontend.layouts.partials.dropdown', $item)
            @endforeach

        </ul>
    </li>
@else
    <li>
        <a class="" id="menu-{{$item}}" href="{{asset($item)}}">{{$label}}</a>
    </li>
@endif
