<section id="slide" class="slider-section">
    <div id="slider-item" class="slider-item-details">
        @foreach($slides as $slide)
            <div class="slider-area slider-bg-5 relative-position" style="background: none;">

                <div class="bg-image @if($slide->overlay == 1) overlay  @endif"
                     style="background-image: url({{asset('storage/uploads/'.$slide->bg_image)}})"></div>
                @php $content = json_decode($slide->content) @endphp
                <div class="slider-text">

                    <div class="section-title mb20 headline text-center ">
                        @if($content->sub_text)
                            <div class="layer-1-1">
                                <span class="subtitle text-uppercase">Création d’effets visuels de qualité</span>
                                <span class="subtitle text-uppercase">Meilleur préparation à des métiers recherchés</span>
                            </div>
                        @endif
                        @if($content->hero_text)
                            <div class="layer-1-3">
                                <h2><span>Découvrir de nouvelles choses</span></h2>
                            </div>
                        @endif
                    </div>
                    @if(isset($content->widget))
                        <div class="layer-1-3">
                            @if($content->widget->type == 1)
                                <div class="search-formation mb30 relative-position">
                                    <form action="{{route('search')}}" method="get">
                                        <input class="formation" name="q" type="text"
                                               placeholder="@lang('labels.frontend.layouts.partials.search_placeholder')">
                                        <div class="nws-button text-center  gradient-bg text-capitalize">
                                            <button type="submit" value="Submit">@lang('labels.frontend.layouts.partials.search_formations')</button>
                                        </div>
                                    </form>
                                </div>
                            @endif


                        </div>
                    @endif
                    @if(isset($content->buttons))
                        <div class="layer-1-4">
                            <div class="about-btn text-center">
                                @foreach($content->buttons as $button)
                                    <div class="genius-btn text-center text-uppercase ul-li-block bold-font">
                                        <a href="{{$button->link}}">{{$button->label}} <i
                                                    class="fas fa-caret-right"></i></a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif


                </div>
            </div>
        @endforeach
    </div>
</section>
<!-- End of slider section
============================================= -->

