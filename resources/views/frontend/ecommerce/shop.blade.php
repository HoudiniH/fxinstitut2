@extends('frontend.layouts.app')

@section('content')
    @include('frontend.ecommerce.partials._carousel')
    <div class="row">
        @if(isset($products))
            @include('frontend.ecommerce.includes._products')
        @else
            <h1>No Products are available</h1>
        @endif
    </div><!-- /.row -->

@endsection
