@extends('frontend.layouts.app')

@section('content')
    @include('frontend.ecommerce.partials._messages')
    <order-page :cart="cart"
                :stage="'{{ $stage }}'"
                :user_order="'{{ $order }}'">
    </order-page>
@endsection

@section('scripts')
    <script src="https://checkout.stripe.com/checkout.js"></script>
@endsection
