@extends('frontend.layouts.app')

@section('title', trans('labels.frontend.home.title').' | '.app_name())
@section('meta_description', '')
@section('meta_keywords','')


@push('after-styles')
    <style>
        /*.address-details.ul-li-block{*/
        /*line-height: 60px;*/
        /*}*/
        .teacher-img-content .teacher-social-name {
            max-width: 67px;
        }

        .my-alert {
            position: absolute;
            z-index: 10;
            left: 0;
            right: 0;
            top: 25%;
            width: 50%;
            margin: auto;
            display: inline-block;
        }

    </style>
@endpush

@section('content')

    <!-- Start of slider section
            ============================================= -->
    @if(session()->has('alert'))
        <div class="alert alert-light alert-dismissible fade my-alert show">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{session('alert')}}</strong>
        </div>
    @endif
    @include('frontend.layouts.partials.slider')

    {{-- //////  HEADER PRODUCT /////// --}}
    <section id="header-product" class="float-left" style="width: 22%">
        <div class="slide-product">
            <img src="{{ asset('/storage/uploads/pdts/accessory-analog-classic.jpg')}}" alt="">
            <div class="mx-auto">
                <span class="text-center">Lorem ipsum, dolor sit amet consectetur</span>
            </div>
        </div>
    </section>


    @if($sections->search_section->status == 1)
        <!-- End of slider section
            ============================================= -->
        <section id="search-formation" class="search-formation-section">
            <div class="container">
                <div class="section-title mb20 headline text-center ">
                    <span class="subtitle text-uppercase">@lang('labels.frontend.home.learn_new_skills')</span>
                    <h2>@lang('labels.frontend.home.search_formations')</h2>
                </div>
                <div class="search-formation mb30 relative-position ">
                    <form action="{{route('search')}}" method="get">

                        <div class="input-group search-group">
                            <input class="formation" name="q" type="text"
                                   placeholder="@lang('labels.frontend.home.search_formation_placeholder')">
                            <select name="category" class="select form-control">
                                @if(count($categories) > 0 )
                                    <option value="">@lang('labels.frontend.formation.select_category')</option>
                                    @foreach($categories as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>

                                    @endforeach
                                @else
                                    <option>>@lang('labels.frontend.home.no_data_available')</option>
                                @endif

                            </select>
                            <div class="nws-button position-relative text-center  gradient-bg text-capitalize">
                                <button type="submit"
                                        value="Submit">@lang('labels.frontend.home.search_formation')</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>
        <!-- End of Search Formations
            ============================================= -->
    @endif


    @if($sections->popular_formations->status == 1)
        @include('frontend.layouts.partials.popular_formations')
    @endif


    @if($sections->latest_news->status == 1)
        <!-- Start latest section
        ============================================= -->
        @include('frontend.layouts.partials.latest_news')
        <!-- End latest section
            ============================================= -->
    @endif


    @if($sections->featured_formations->status == 1)
        <!-- Start of best formation
        ============================================= -->
        {{--@include('frontend.layouts.partials.browse_formations')--}}
        <!-- End of best formation
            ============================================= -->
    @endif


    @if($sections->teachers->status == 1)
        <!-- Start of formation teacher
        ============================================= -->
        <section id="formation-teacher" class="formation-teacher-section">
            <div class="jarallax">
                <div class="container">
                    <div class="section-title mb20 headline text-center ">
                        <span class="subtitle text-uppercase">@lang('labels.frontend.home.our_professionals')</span>
                        <h2>{{env('APP_NAME')}} <span>@lang('labels.frontend.home.teachers').</span></h2>
                    </div>

                    <div class="teacher-list">
                        <div class="row justify-content-center">
                            <!-- /teacher -->
                            @if(count($teachers)> 0)
                                @foreach($teachers as $item)
                                    <div class="col-md-3">
                                        <div class="teacher-img-content ">
                                            <div class="teacher-cntent">
                                                <div class="teacher-social-name ul-li-block">
                                                    <ul>
                                                        <li><a href="{{'mailto:'.$item->email}}"><i
                                                                        class="fa fa-envelope"></i></a></li>
                                                        <li>
                                                            <a href="{{route('admin.messages',['teacher_id'=>$item->id])}}"><i
                                                                        class="fa fa-comments"></i></a></li>
                                                    </ul>
                                                    <div class="teacher-name">
                                                        <span>{{$item->full_name}}</span>
                                                    </div>
                                                </div>
                                                <div class="teacher-img-category">
                                                    <div class="teacher-img">
                                                        <img src="{{$item->picture}}" style="height: 100%" alt="">
                                                        {{--<div class="formation-price text-uppercase text-center gradient-bg">--}}
                                                        {{--<span>Featured</span>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                    {{--<div class="teacher-category float-right">--}}
                                                    {{--<span class="st-name">{{$item->name}} </span>--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>

                        <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font ">
                            <a href="{{route('teachers.index')}}">@lang('labels.frontend.home.all_teachers')<i
                                        class="fas fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End of formation teacher
            ============================================= -->
    @endif


    @if($sections->faq->status == 1)
        <!-- Start FAQ section
        ============================================= -->
        @include('frontend.layouts.partials.faq')
        <!-- End FAQ section
            ============================================= -->
    @endif


    @if($sections->formation_by_category->status == 1)
        <!-- Start Formation category
        ============================================= -->
        @include('frontend.layouts.partials.formation_by_category')
        <!-- End Formation category
            ============================================= -->
    @endif


    @if($sections->contact_us->status == 1)
        <!-- Start of contact area
        ============================================= -->
        @include('frontend.layouts.partials.contact_area')
        <!-- End of contact area
            ============================================= -->
    @endif


@endsection

@push('after-scripts')
    <script>
        $('ul.product-tab').find('li:first').addClass('active');
    </script>
@endpush
