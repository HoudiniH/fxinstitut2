@extends('frontend.layouts.app')
@section('title', trans('labels.frontend.formation.formations').' | '. app_name() )

@push('after-styles')
    <style>
        .couse-pagination li.active {
            color: #333333 !important;
            font-weight: 700;
        }

        .page-link {
            position: relative;
            display: block;
            padding: .5rem .75rem;
            margin-left: -1px;
            line-height: 1.25;
            color: #c7c7c7;
            background-color: #1d1e22;
            border: none;
        }

        .page-item.active .page-link {
            z-index: 1;
            color: #333333;
            background-color: #fdd765 ;
            border: none;

        }
     .listing-filter-form select{
            height:50px!important;
        }

        ul.pagination {
            display: inline;
            text-align: center;
        }
    </style>
@endpush
@section('content')

    <!-- Start of breadcrumb section
        ============================================= -->
    <section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
        <div class="blakish-overlay"></div>
        <div class="container">
            <div class="page-breadcrumb-content text-center">
                <div class="page-breadcrumb-title">
                    <h2 class="breadcrumb-head black bold">
                        <span>@if(isset($category)) {{$category->name}} @else @lang('labels.frontend.formation.formations') @endif </span>
                    </h2>
                </div>
            </div>
        </div>
    </section>
    <!-- End of breadcrumb section
        ============================================= -->


    <!-- Start of tutorial section
        ============================================= -->
    <section id="tutorial-page" class="tutorial-page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-dismissable alert-success fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('success')}}
                        </div>
                    @endif
                    <div class="short-filter-tab">
                        <div class="shorting-filter w-50 d-inline float-left mr-3">
                            <span>@lang('labels.frontend.formation.sort_by')</span>
                            <select id="sortBy" class="form-control d-inline w-50">
                                <option value="">@lang('labels.frontend.formation.none')</option>
                                <option value="popular">@lang('labels.frontend.formation.popular')</option>
                                <option value="trending">@lang('labels.frontend.formation.trending')</option>
                                <option value="featured">@lang('labels.frontend.formation.featured')</option>
                            </select>
                        </div>

                        <div class="tab-button blog-button ul-li text-center float-right">
                            <ul class="product-tab">
                                <li class="active" rel="tab1"><i class="fas fa-th"></i></li>
                                <li rel="tab2"><i class="fas fa-list"></i></li>
                            </ul>
                        </div>

                    </div>

                    <div class="genius-post-item">
                        <div class="tab-container">
                            <div id="tab1" class="tab-content-1 pt35">
                                <div class="best-tutorial-area best-tutorial-v2">
                                    <div class="row">
                                        @if($tutorials->count() > 0)

                                            @foreach($tutorials as $tutorial)

                                                <div class="col-md-4">
                                                    <div class="best-tutorial-pic-text relative-position">
                                                        <div class="best-tutorial-pic relative-position"
                                                             @if($tutorial->image) style="background-image: url('{{asset('storage/uploads/tols/'.$tutorial->image->name)}}')" @endif>

                                                            @if($tutorial->trending == 1)
                                                                <div class="trend-badge-2 text-center text-uppercase">
                                                                    <i class="fas fa-bolt"></i>
                                                                    <span>@lang('labels.frontend.badges.trending')</span>
                                                                </div>
                                                            @endif
                                                                @if($tutorial->free == 1)
                                                                    <div class="trend-badge-3 text-center text-uppercase">
                                                                        <i class="fas fa-bolt"></i>
                                                                        <span>@lang('labels.backend.formations.fields.free')</span>
                                                                    </div>
                                                                @endif
                                                            <div class="tutorial-price text-center gradient-bg">
                                                                @if($tutorial->free == 1)
                                                                    <span>{{trans('labels.backend.formations.fields.free')}}</span>
                                                                @else
                                                                    <span> {{$appCurrency['symbol'].' '.$tutorial->price}}</span>
                                                                @endif
                                                            </div>

                                                            <div class="tutorial-rate ul-li">
                                                                <ul>
                                                                    @for($i=1; $i<=(int)$tutorial->rating; $i++)
                                                                        <li><i class="fas fa-star"></i></li>
                                                                    @endfor
                                                                </ul>
                                                            </div>
                                                            <div class="tutorial-details-btn">

                                                                <a href="{{ route('tutorials.show', ['slug' => $tutorial->slug] ) }}">@lang('labels.frontend.formation.formation_detail')
                                                                    <i class="fas fa-arrow-right"></i></a>

                                                            </div>
                                                            <div class="blakish-overlay"></div>
                                                        </div>
                                                        <div class="best-tutorial-text">
                                                            <div class="tutorial-title mb20 headline relative-position">
                                                                <h3>
                                                                    <a href="{{ route('tutorials.show', ['slug' => $tutorial->slug]) }}">{{$tutorial->title}}</a>
                                                                </h3>

                                                            </div>
                                                            <div class="tutorial-meta">
                                                                <span class="tutorial-category"><a
                                                                            href="{{route('tutorials.category',['category'=>$tutorial->category->slug])}}">{{$tutorial->category->name}}</a></span>
                                                                <span class="tutorial-author"><a href="#">{{ $tutorial->students()->count() }}
                                                                        @lang('labels.frontend.formation.students')</a></span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        @else
                                            <h3>@lang('labels.general.no_data_available')</h3>

                                    @endif

                                    <!-- /tutorial -->

                                    </div>
                                </div>
                            </div><!-- /tab-1 -->

                            <div id="tab2" class="tab-content-1">
                                <div class="tutorial-list-view">
                                    <table>
                                        <tr class="list-head">
                                            <th>@lang('labels.frontend.formation.tutorial_name')</th>
                                            <th>@lang('labels.frontend.formation.tutorial_type')</th>
                                            <th>@lang('labels.frontend.formation.starts')</th>
                                        </tr>

                                        @if($tutorials->count() > 0)

                                            @foreach($tutorials as $tutorial)

                                                <tr>
                                                    <td>
                                                        <div class="tutorial-list-img-text">
                                                            <div class="tutorial-list-img"
                                                                 @if($tutorial->image ) style="background-image: url({{asset('storage/uploads/tols/'.$tutorial->image->name)}})" @endif >
                                                            </div>
                                                            <div class="tutorial-list-text">
                                                                <h3>
                                                                    <a href="{{ route('tutorials.show', [ 'slug' => $tutorial->slug]) }}">{{$tutorial->title}}</a>
                                                                </h3>
                                                                <div class="tutorial-meta">
                                                                <span class="tutorial-category bold-font"><a
                                                                            href="{{ route('tutorials.show', ['slug' => $tutorial->slug]) }}">
                                                                        @if($tutorial->free == 1)
                                                                            {{trans('labels.backend.tutorials.fields.free')}}
                                                                        @else
                                                                            {{$appCurrency['symbol'].' '.$tutorial->price}}
                                                                        @endif
                                                                    </a></span>

                                                                    <div class="tutorial-rate ul-li">
                                                                        <ul>
                                                                            @for($i=1; $i<=(int)$tutorial->rating; $i++)
                                                                                <li><i class="fas fa-star"></i></li>
                                                                            @endfor
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="tutorial-type-list">
                                                            <span><a href="{{route('tutorials.category',['category'=>$tutorial->category->slug])}}">{{$tutorial->category->name}}</a></span>
                                                        </div>
                                                    </td>
                                                    <td>{{\Carbon\Carbon::parse($tutorial->start_date)->format('d M Y')}}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3">
                                                    <h3>@lang('labels.general.no_data_available')</h3>

                                                </td>
                                            </tr>
                                        @endif

                                    </table>
                                </div>
                            </div><!-- /tab-2 -->
                        </div>
                        <div class="couse-pagination text-center ul-li">
                            {{ $tutorials->links() }}
                        </div>
                    </div>


                </div>
                {{-- ////  SIDEBAR ///// --}}

                {{-- <div class="col-md-3">
                    <div class="side-bar">

                        <div class="side-bar-widget  first-widget">
                            <h2 class="widget-title text-capitalize">@lang('labels.frontend.formation.find_your_tutorial')</h2>
                            <div class="listing-filter-form pb30">
                                <form action="{{route('search-tutorial')}}" method="get">

                                    <div class="filter-search mb20">
                                        <label class="text-uppercase">@lang('labels.frontend.formation.category')</label>
                                        <select name="category" class="form-control listing-filter-form select">
                                            <option value="">@lang('labels.frontend.formation.select_category')</option>
                                            @if(count($categories) > 0)
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>

                                                @endforeach
                                            @endif

                                        </select>
                                    </div>


                                    <div class="filter-search mb20">
                                        <label>@lang('labels.frontend.formation.full_text')</label>
                                        <input type="text" class="" name="q" placeholder="{{trans('labels.frontend.formation.looking_for')}}">
                                    </div>
                                    <button class="genius-btn gradient-bg text-center text-uppercase btn-block text-white font-weight-bold"
                                            type="submit">@lang('labels.frontend.formation.find_tutorials') <i
                                                class="fas fa-caret-right"></i></button>
                                </form>

                            </div>
                        </div>

                        @if($recent_news->count() > 0)
                            <div class="side-bar-widget">
                                <h2 class="widget-title text-capitalize">@lang('labels.frontend.formation.recent_news')</h2>
                                <div class="latest-news-posts">
                                    @foreach($recent_news as $item)
                                        <div class="latest-news-area">

                                            @if($item->image != "")
                                                <div class="latest-news-thumbnile relative-position"
                                                     style="background-image: url({{asset('storage/uploads/'.$item->image)}})">
                                                    <div class="blakish-overlay"></div>
                                                </div>
                                            @endif
                                            <div class="date-meta">
                                                <i class="fas fa-calendar-alt"></i> {{$item->created_at->format('d M Y')}}
                                            </div>
                                            <h3 class="latest-title bold-font"><a
                                                        href="{{route('blogs.index',['slug'=>$item->slug.'-'.$item->id])}}">{{$item->title}}</a>
                                            </h3>
                                        </div>
                                        <!-- /post -->
                                    @endforeach


                                    <div class="view-all-btn bold-font">
                                        <a href="{{route('blogs.index')}}">@lang('labels.frontend.formation.view_all_news')
                                            <i class="fas fa-chevron-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>

                        @endif


                        @if($global_featured_tutorial != "")
                            <div class="side-bar-widget">
                                <h2 class="widget-title text-capitalize">@lang('labels.frontend.formation.featured_tutorial')</h2>
                                <div class="featured-tutorial">
                                    <div class="best-tutorial-pic-text relative-position pt-0">
                                        <div class="best-tutorial-pic relative-position "
                                             @if($global_featured_tutorial->tutorial_image != "") style="background-image: url({{asset('storage/uploads/'.$global_featured_tutorial->tutorial_image)}})" @endif>

                                            @if($global_featured_tutorial->trending == 1)
                                                <div class="trend-badge-2 text-center text-uppercase">
                                                    <i class="fas fa-bolt"></i>
                                                    <span>@lang('labels.frontend.badges.trending')</span>
                                                </div>
                                            @endif
                                                @if($global_featured_tutorial->free == 1)
                                                    <div class="trend-badge-3 text-center text-uppercase">
                                                        <i class="fas fa-bolt"></i>
                                                        <span>@lang('labels.backend.tutorials.fields.free')</span>
                                                    </div>
                                                @endif

                                        </div>
                                        <div class="best-tutorial-text" style="left: 0;right: 0;">
                                            <div class="tutorial-title mb20 headline relative-position">
                                                <h3>
                                                    <a href="{{ route('tutorials.show', ['slug' => $global_featured_tutorial->slug]) }}">{{$global_featured_tutorial->title}}</a>
                                                </h3>
                                            </div>
                                            <div class="tutorial-meta">
                                                <span class="tutorial-category"><a
                                                            href="{{route('tutorials.category',['category'=>$global_featured_tutorial->category->slug])}}">{{$global_featured_tutorial->category->name}}</a></span>
                                                <span class="tutorial-author">{{ $global_featured_tutorial->students()->count() }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
    <!-- End of tutorial section
        ============================================= -->

    {{-- <!-- Start of best tutorial
   =============================================  -->
    @include('frontend.layouts.partials.browse_tutorials')
    <!-- End of best tutorial
            ============================================= --> --}}


@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $(document).on('change', '#sortBy', function () {
                if ($(this).val() != "") {
                    location.href = '{{url()->current()}}?type=' + $(this).val();
                } else {
                    location.href = '{{route('tutorials.all')}}';
                }
            })

            @if(request('type') != "")
            $('#sortBy').find('option[value="' + "{{request('type')}}" + '"]').attr('selected', true);
            @endif
        });

    </script>
@endpush
