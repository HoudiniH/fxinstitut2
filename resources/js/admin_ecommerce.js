/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Event = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue';
import {
    ValidationObserver,
    ValidationProvider,
    extend,
    validate
} from 'vee-validate';

import {
    required,
    email,
    alpha_spaces,
    min,
    confirmed,
    regex,
    between,
    alpha,
    url,
    image
} from "vee-validate/dist/rules";


// Override the default message.
extend('required', {
    ...required,
    message: 'Veuillez remplir ce champ'
});
// Override the default message.
extend('email', {
    ...email,
    message: `L'email n'est pas valide`,
});
// Override the default message.
extend('alpha_spaces', {
    ...alpha_spaces,
    message: `Ce champ n'est pas valide`
});
// Override the default message.
extend('min', {
    ...min,
    message: "Le {_field_} n'est pas valide",
});

extend('confirmed', {
    ...confirmed,
    message: "La confirmation du {_field_} ne correspond pas"
});
extend('regex', regex);
extend('between', between);
extend('alpha', alpha);
extend('image', image);
extend('url', {
    validate(value) {
        if (value) {
            return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);
        }

        return false;
    },
    message: 'This value must be a valid URL',
})

window._ = require('lodash');

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);



/**
 * Add vue components to the project.
 */
Vue.component('admin-main-page', require('./components/admin-main-page.vue').default);

// Products
Vue.component('product-table', require('./components/admin/products/product-table.vue').default);
Vue.component('product-row', require('./components/admin/products/product-row.vue').default);
Vue.component('delete-product-form', require('./components/admin/products/delete-product-form.vue').default);
Vue.component('product-form', require('./components/admin/products/product-form.vue').default);
Vue.component('view-product', require('./components/admin/products/view-product.vue').default);

// Sales
Vue.component('sale-form', require('./components/admin/sales/sale-form.vue').default);
Vue.component('view-sale-details', require('./components/admin/sales/view-sale-details.vue').default);

// Functionality
Vue.component('form-modal', require('./components/functionality/modal/form-modal.vue').default);
Vue.component('view-message', require('./components/functionality/view-message.vue').default);
Vue.component('message-modal', require('./components/functionality/modal/message-modal.vue').default);
Vue.component('confirm-modal', require('./components/functionality/modal/confirm-modal.vue').default);
Vue.component('view-details-modal', require('./components/functionality/modal/view-details-modal.vue').default);
Vue.component('paginate-items', require('./components/functionality/paginate-items.vue').default);
Vue.component('input-date', require('./components/functionality/form/input-date.vue').default);
Vue.component('success-message', require('./components/functionality/messages/success-message.vue').default)
Vue.component('error-message', require('./components/functionality/messages/error-message.vue').default);
Vue.component('full-screen', require('./components/functionality/screen/full-screen.vue').default);

// Animation
Vue.component('fade-out-animation', require('./components/functionality/animations/fade-out-animation.vue').default);
Vue.component('slide-out-animation', require('./components/functionality/animations/slide-in-animation.vue').default);


// Orders
Vue.component('view-order', require('./components/admin/orders/view-order.vue').default);
Vue.component('ship-order', require('./components/admin/orders/ship-order.vue').default);
Vue.component('user-order-item', require('./components/order/confirm-cart/user-order-item.vue').default);
Vue.component('user-order-header', require('./components/order/shared/user-order-header.vue').default);
Vue.component('order-total-details', require('./components/order/shared/order-total-details.vue').default);
Vue.component('shipping-address', require('./components/shared-information/address/shipping-address.vue').default);
Vue.component('user-details', require('./components/shared-information/user-details.vue').default);

// Categories
Vue.component('categories-table', require('./components/admin/categories/categories-table.vue').default);
Vue.component('category-row', require('./components/admin/categories/category-row.vue').default);
Vue.component('category-form', require('./components/admin/categories/category-form.vue').default);
Vue.component('edit-category-button', require('./components/admin/categories/edit-category-button.vue').default);
Vue.component('delete-category-button', require('./components/admin/categories/delete-category-button.vue').default);

// Taxes
Vue.component('tax-table', require('./components/admin/taxes/tax-table.vue').default);
Vue.component('tax-row', require('./components/admin/taxes/tax-row.vue').default);
Vue.component('tax-form', require('./components/admin/taxes/tax-form.vue').default);
Vue.component('delete-tax-button', require('./components/admin/taxes/delete-tax-button.vue').default);

// States
Vue.component('states-table', require('./components/admin/states/states-tables.vue').default);
Vue.component('state-row', require('./components/admin/states/state-row.vue').default);
Vue.component('state-form', require('./components/admin/states/states-form.vue').default);

const app = new Vue({
    el: '#app'
});
